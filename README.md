# Moved to GitHub

This module migrated to https://github.com/atlassian/virtual-users due to [JPERF-375](https://ecosystem.atlassian.net/browse/JPERF-375).